<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 08.10.19
 * Time: 13:54
 */

namespace models;

use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\RFCValidation;
use voku\helper\AntiXSS;

class Datvalidate
{


    public function valid_email($email)
    {
        $email= $this->clean($email);
        $validator = new EmailValidator();

        if ($validator->isValid($email, new RFCValidation())) {
            return $email;
        } else {
            return false;
        }

    }


    public function name_user($name)
    {
        $name=$this->clean($name);

        if (preg_match("/^[A-Za-z0-9]+$/", $name)) {

            return $name;

        } else {

            return false;
        }

    }

    public function url_user($url)
    {


        if (parse_url($url, PHP_URL_SCHEME) && parse_url($url, PHP_URL_HOST)) {
            return $url;
        } else {
            return false;
        }
    }


    public function text_user($textarea)
    {

        $antiXss = new AntiXSS();
        $textarea = $antiXss->xss_clean($textarea);
        $textarea = htmlspecialchars(strip_tags($textarea, '<a><code><strong><i>'), ENT_QUOTES, 'UTF-8');
        return $textarea;

    }

    public function clean($text)
    {
        $antiXss = new AntiXSS();
        $text = $antiXss->xss_clean($text);
        $text = htmlspecialchars(strip_tags($text), ENT_QUOTES, 'UTF-8');
        return $text;

    }


    public function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function id_post($id_post)
    {
        $id_post=$this->clean($id_post);

        if (preg_match("/^[0-9]+$/", $id_post)) {

            return $id_post;

        } else {

            return false;
        }
    }
}