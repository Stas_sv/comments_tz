<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 09.10.19
 * Time: 21:20
 */


namespace models;


use Gumlet\ImageResize;

class FileUpload
{


    public function file_handler($files_name, $files_type, $files_tmp_name)
    {


        $ext = pathinfo($files_name, PATHINFO_EXTENSION);
        $uploaddir = __DIR__ . '/../file/';
        $uploadfile = $uploaddir . md5($files_name) . '.' . $ext;
        try {
            move_uploaded_file($files_tmp_name, $uploadfile);
        } catch (Exception $e) {
            echo 'Поймано исключение: ', $e->getMessage(), "\n";
        }


        switch ($files_type) {
            case 'text/plain':

                $file_url = 'file/' . md5($files_name) . '.' . $ext;

                break;


            default:
                $size = (getimagesize($uploadfile));
                $size = explode(' ', $size[3]);
                $size = preg_replace('/[^0-9]/', '', $size);

                if ($size > 320) {
                    $image = new  ImageResize ($_SERVER['DOCUMENT_ROOT'] . '/file/' . md5($files_name) . '.' . $ext);
                    $image->resizeToBestFit(320, 420);
                    $image->save($_SERVER['DOCUMENT_ROOT'] . '/file/' . md5(md5($files_name)) . '.' . $ext);
                    unlink($_SERVER['DOCUMENT_ROOT'] . '/file/' . md5($files_name) . '.' . $ext);
                    $file_url = 'file/' . md5(md5($files_name)) . '.' . $ext;
                }

        }
        $file_url = $file_url ?? '';
        return $file_url;
    }


}