<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 02.10.19
 * Time: 21:08
 */

namespace models;

use PDO, PDOException;


class Dbc
{

    private $host;
    private $user;
    private $password;
    private $dbname;
    private $options;

    public function __construct()
    {


        $this->host = '127.0.0.1';
        $this->user = 'root';
        $this->password = 'root';
        $this->dbname = 'comments_tz';
        $this->options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_CASE => PDO::CASE_NATURAL,
            PDO::ATTR_ORACLE_NULLS => PDO::NULL_EMPTY_STRING
        ];
    }

    public function connect()
    {


        try {

            $connection = new PDO('mysql:host=' . $this->host . '; dbname=' . $this->dbname . ';charset=utf8', $this->user, $this->password, $this->options);


        } catch (PDOException $e) {
            die("Database connection failed: " . $e->getMessage());
        }
        return $connection;
    }

    public function disconnect()
    {
    }


    public function select()
    {

        $data = $this->connect()->query('SELECT * FROM post ORDER by id_parent ASC,id ASC')->fetchAll(PDO::FETCH_ASSOC);
        return $data;


    }

    public function insert_post_main($post_email, $post_name, $post_textarea, $browser, $ip_user, $file_url = '', $id_parent = '0', $post_url = '')
    {
        $date = date('Y-m-d H:i:s');
        try {
            $con = $this->connect()->prepare('INSERT INTO post (user_name,email,home_page,text,date,ip,browser,id_parent,file)  VALUES (:user_name,:email,:home_page,:text,:date,:ip,:browser,:id_parent,:file)');
            $con->bindParam(':user_name', $post_name, PDO::PARAM_STR);
            $con->bindParam(':email', $post_email, PDO::PARAM_STR);
            $con->bindParam(':home_page', $post_url, PDO::PARAM_STR);
            $con->bindParam(':text', $post_textarea, PDO::PARAM_STR);
            $con->bindParam(':date', $date, PDO::PARAM_STR);
            $con->bindParam(':ip', $ip_user, PDO::PARAM_STR);
            $con->bindParam(':browser', $browser, PDO::PARAM_STR);
            $con->bindParam(':id_parent', $id_parent, PDO::PARAM_STR);
            $con->bindParam(':file', $file_url, PDO::PARAM_STR);
            $con->execute();

        } catch (Exception $e) {
            echo 'Поймано исключение: ', $e->getMessage(), "\n";
        }

    }


}