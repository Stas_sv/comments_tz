<?php


 function first() {
    $host='127.0.0.1';
    $user='root';
    $password='root';
    $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_CASE => PDO::CASE_NATURAL,
        PDO::ATTR_ORACLE_NULLS => PDO::NULL_EMPTY_STRING
    ];
    $data= new PDO("mysql:host=$host", $user, $password,$options);

    try {


        $sql = "CREATE DATABASE IF NOT EXISTS comments_tz CHARACTER SET utf8 COLLATE utf8_general_ci";
        $data->exec($sql);
        $sql = "use comments_tz";
        $data->exec($sql);
        $sql = "CREATE TABLE IF NOT EXISTS post (
  id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  user_name VARCHAR(30),
  email VARCHAR(60),
  home_page VARCHAR(110),
  text VARCHAR(6000),
  date VARCHAR(60),
  ip VARCHAR(60),
  browser VARCHAR(100),
  id_parent VARCHAR(60),
  file VARCHAR(100))";
        $data->exec($sql);

    }
    catch(PDOException $e)
    {
        echo $sql . "<br>" . $e->getMessage();
    } finally{echo 'OK! БД и таблица созданы. Пропишите данные для подключения в ...../public_html/models/Dbc.php';}
}

first();