<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 10.10.19
 * Time: 14:33
 */

function protocol()
{
    $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 5)) == 'https' ? 'https' : 'http';
    if ($_SERVER["SERVER_PORT"] == 443)
        $protocol = 'https';
    elseif (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1')))
        $protocol = 'https';
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on')
        $protocol = 'https';
    return $protocol;
}


function format_sort($arr)
{

    for ($i = 0, $c = count($arr); $i < $c; $i++) {
        $new_arr[$arr[$i]['id_parent']][] = $arr[$i];
    }

    return $new_arr;
}

function my_sort($data, $parent = 0, $level = 0)
{
    $aa = $aa ?? 0;
    $protocol = protocol();
    $arr = $data[$parent];
    for ($i = 0; $i < count($arr); $i++) {
        $html = bbcode_to_html($arr[$i]['text']);

        if ($level == 0) {
            $datanum = 'data-num="' . $aa++ . '"';
            $num = 'num';
        } else {
            $datanum = '';
            $num = '';

        }

        echo '<div class="card card' . $level . ' ' . $num . '" ' . $datanum . ' style="margin-left:' . $level . 'px;" >
  <div class="card-header d-flex justify-content-between">
 <span class="d-flex align-items-start flex-column"><span class="item-name">' . $arr[$i]['user_name'] . '</span><span class="badge badge-primary text-wrap item-nameemail">' . $arr[$i]['email'] . '</span></span> <span> <span class="item-date' . $level . '">' . $arr[$i]['date'] . '</span><button class="btn ml-1  btn-outline-dark btn-sm answ" data-idpost="' . $arr[$i]['id'] . '"  >Answer</button></span>
  </div>
  <div class="card-body">
   
    <p class="card-text">' . $html . '</p>';
        if (!empty($arr[$i]['file'])) {
            echo '
    <p> <a href="' . $protocol . '://' . $_SERVER['HTTP_HOST'] . '/' . $arr[$i]['file'] . '"> 
Attached file</a></p>';
        }

        echo '</div>';

        if (isset($data[$arr[$i]['id']])) my_sort($data, $arr[$i]['id'], 20);
        echo '</div>';

    }


}


function bbcode_to_html($bbtext)
{
    $bbtags = array(


        '[i]' => '<span style="font-style: italic;">', '[/i]' => '</span>',

        '[b]' => '<span style="font-weight:bolder;">', '[/b]' => '</span>',


        '[code]' => '<code>', '[/code]' => '</code>',

    );

    $bbtext = str_ireplace(array_keys($bbtags), array_values($bbtags), $bbtext);

    $bbextended = array(
        "/\[url](.*?)\[\/url]/i" => "<a href=\"http://$1\" title=\"$1\">$1</a>",
        "/\[url=(.*?)\](.*?)\[\/url\]/i" => "<a href=\"$1\" title=\"$1\">$2</a>",


    );

    foreach ($bbextended as $match => $replacement) {
        $bbtext = preg_replace($match, $replacement, $bbtext);
    }
    return $bbtext;
}