<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 02.10.19
 * Time: 23:06
 */
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once __DIR__ . '/../vendor/autoload.php';


use models\Datvalidate;
use models\Dbc;
use models\FileUpload;

if (isset($_POST)) {


    $data_us = new Datvalidate();

    $post_email = $data_us->valid_email($_POST['email']);

    $post_name = $data_us->name_user($_POST['name']);

    $post_url = $data_us->url_user($_POST['url']) ?? '';

    $post_textarea = $data_us->text_user($_POST['textarea']);

    $id_parent=$data_us->id_post($_POST['id_parent']);


    $browser = implode(',', parse_user_agent());

    $ip_user = $data_us->getRealIpAddr();


    if (isset($_FILES['file']['name']) && ($_FILES['file']['size']>0)) {


        $files_name = $_FILES['file']['name'] ?? '';

        $files_type = $_FILES['file']['type'] ?? '';

        $files_tmp_name = $_FILES['file']['tmp_name'] ?? '';

        $files_size = $_FILES['file']['size'] ?? '';

        $arr = array('image/gif', 'image/jpeg', 'image/png', 'text/plain');


        if (($files_size < 10485760)) {

            $url_f = new FileUpload();
            $file_url = $url_f->file_handler($files_name, $files_type, $files_tmp_name);

        }
    }
    $file_url= $file_url??'';
    $id_parent = $id_parent ?? 0;
    if (isset($post_email, $post_name)) {

        $insert = new Dbc();
        try {
            $insert->insert_post_main($post_email, $post_name, $post_textarea, $browser, $ip_user, $file_url, $id_parent, $post_url = '');
        } catch (Exception $e) {
            echo 'Поймано исключение: ', $e->getMessage(), "\n";
        }


    }

}

