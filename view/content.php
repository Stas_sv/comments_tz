

</div>
<div class="paginator" onclick="pagination(event)"></div>
<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Preview</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card"  id="modapreview" style="margin-left: 0px; display: block;">
                    <div class="card-header d-flex justify-content-between">
                        <span class="d-flex align-items-start flex-column"><span class="item-name"> </span><span class="badge badge-primary text-wrap item-nameemail"> </span></span> <span> <span class="item-date0"><?php echo date('Y-m-d ')?></span></span>
                    </div>
                    <div class="card-body">

                        <p class="card-text"> </p></div></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
</div>

<div class="container col-4 offset-1">
    <div class="row">


        <form id="form_text" method="post" action="index.php" enctype="multipart/form-data" class="position-fixed">
            <input type="text" class="form-control" id="id_parent" name="id_parent" value="0" hidden >
            <div class="form-group">
                <label for="email">Email address</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" maxlength="60" required>
            </div>
            <div class="form-group">
                <label for="name">User name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="enter you nick" maxlength="30" required>
            </div>
            <div class="form-group">
                <label for="url">Home page</label>
                <input type="url" class="form-control" id="url" name="url" placeholder="URL" maxlength="100">
            </div>

            <div class="form-group">

                <input type="file" class="form-control-file" name="file" id="file" accept="image/gif, image/jpeg, image/png, text/plain">
            </div>
            <div class="form-group">
                <button class="btn btn-danger" id="clean_id"  > I do not want to answer</button>
                <label for="textarea" id="new_text">Write a new message </label>
                <textarea class="form-control" id="editor"   name="textarea" rows="3"></textarea>
            </div>
            <div class="form-group">
                <label for="phrase">CAPCHA</label>

                <div id="captcha">
                </div>

                <input class="form-control" type="text" placeholder="Captcha" id="cpatchaTextBox" required />
            </div>

            <input type="submit" value="Upload mess" class="btn btn-success" name="submit">
            <button type="button" class="btn btn-primary" data-toggle="modal" id="modalpre" data-target="#exampleModal">
                Preview Message
            </button>

        </form>
    </div>


</div>
