$(document).ready(function () {
    $("#clean_id").hide();
    var code;
    function createCaptcha() {
        //clear the contents of captcha div first
        document.getElementById('captcha').innerHTML = "";
        var charsArray =
            "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var lengthOtp = 6;
        var captcha = [];
        for (var i = 0; i < lengthOtp; i++) {
            //below code will not allow Repetition of Characters
            var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
            if (captcha.indexOf(charsArray[index]) == -1)
                captcha.push(charsArray[index]);
            else i--;
        }
        var canv = document.createElement("canvas");
        canv.id = "captcha";
        canv.width = 100;
        canv.height = 50;
        var ctx = canv.getContext("2d");
        ctx.font = "25px Georgia";
        ctx.strokeText(captcha.join(""), 0, 30);
        //storing captcha so that can validate you can save it somewhere else according to your specific requirements
        code = captcha.join("");
        document.getElementById("captcha").appendChild(canv); // adds the canvas to the body element
    }
    $(document).ready(createCaptcha());

    function validateCaptcha() {
        event.preventDefault();

        if (document.getElementById("cpatchaTextBox").value == code) {
            $('#cpatchaTextBox').replaceWith("<div class=\"alert alert-primary\" role=\"alert\">Message sent</div>");
            return true;
        }

    }



    var items = document.querySelectorAll('.card0');

    function sortingByName() {


        Array.from(items).sort(function (a, b) {

            a = a.querySelector('.item-name').innerText.toLowerCase();
            b = b.querySelector('.item-name').innerText.toLowerCase();
            return (a > b) - (a < b)
        }).forEach(function (n, i) {
            n.style.order = i
        })

    }

    function sortingByName2() {


        Array.from(items).sort(function (a, b) {

            a = a.querySelector('.item-name').innerText.toLowerCase();
            b = b.querySelector('.item-name').innerText.toLowerCase();
            return (a < b) - (a > b)
        }).forEach(function (n, i) {
            n.style.order = i
        })

    }

    function sortingByEmail() {


        Array.from(items).sort(function (a, b) {

            a = a.querySelector('.item-nameemail').innerText.toLowerCase();
            b = b.querySelector('.item-nameemail').innerText.toLowerCase();
            return (a > b) - (a < b)
        }).forEach(function (n, i) {
            n.style.order = i
        })


    }

    function sortingByEmail2() {

        Array.from(items).sort(function (a, b) {

            a = a.querySelector('.item-nameemail').innerText.toLowerCase();
            b = b.querySelector('.item-nameemail').innerText.toLowerCase();
            return (a < b) - (a > b)
        }).forEach(function (n, i) {
            n.style.order = i
        })


    }

    function sortingByDate() {


        Array.from(items).sort(function (a, b) {

            var dateA = new Date(a.querySelector('.item-date0').innerText),
                dateB = new Date(b.querySelector('.item-date0').innerText);

            return dateA - dateB
        }).forEach(function (n, i) {
            n.style.order = i;
        })

    }

    function sortingByDate2() {

        Array.from(items).sort(function (a, b) {

            var dateA = new Date(a.querySelector('.item-date0').innerText),
                dateB = new Date(b.querySelector('.item-date0').innerText);

            return dateB - dateA


        }).forEach(function (n, i) {
            n.style.order = i;
        })

    }

    var countemail = 0;
    var countdate = 0;
    var countname = 0;

    $("#sortByEmail").click(function () {

        countemail++;

        countemail % 2 ? sortingByEmail() : sortingByEmail2();
    });

    $("#sortByDate").click(function () {

        countdate++;

        countdate % 2 ? sortingByDate() : sortingByDate2();
    });


    $("#sortByName").click(function () {

        countname++;

        countname % 2 ? sortingByName() : sortingByName2();
    });



    $(document).on("submit", "#form_text", function(event)
    {
        event.preventDefault();
      if (validateCaptcha()) {

          $.ajax({
              url: '../../controllers/controller.php',
              type: $(this).attr("method"),
              dataType: "JSON",
              data: new FormData(this),
              processData: false,
              contentType: false,
              success: function (data, status) {

              },
              error: function (xhr, desc, err) {

              }
          });

      } else {
        alert("Invalid Captcha. try Again");
        createCaptcha();
    }


    });

    var fl = document.getElementById('file');

    fl.onchange = function(e){
        var ext = this.value.match(/\.(.+)$/)[1];
        if (((this.files[0].size >102400) && (ext =='txt')) ||(this.files[0].size >10485760)){this.value=''; alert('Wrong size  file. Max size txt file 100kb, img 10mb ');} else {

            switch (ext) {
                case 'jpg':
                case 'gif':
                case 'png':
                case 'txt':

                    break;
                default:
                    alert('wrong format. only jpg,gif,png,txt');
                    this.value = '';
            }

        }
    };

    var wbbOpt = {
        buttons: "bold,italic,|,link,|,code"
    };
    $("#editor").wysibb(wbbOpt);



    $( ".answ" ).each(function() {
        $(this).on("click", function(){
            let ext = $(this).data('idpost');
           $('.wysibb-text-editor.wysibb-body').focus();
            $('#id_parent').attr('value', ext);
            $('#new_text').text( 'Write the answer').addClass('badge, badge-warning');
            $("#clean_id").show()
        });
    });
    $("#clean_id").click(function () {
        $('#id_parent').attr('value', '0');
        $('#new_text').text( 'Write a new message').removeClass('badge, badge-warning');
        $("#clean_id").hide();
    });

    $("#modalpre").click(function () {

        let name= $("#form_text #name ").val();
        let email=$("#form_text #email ").val();
        let text=$('.wysibb-text-editor.wysibb-body').html();
console.log(name,email,text);

        $('#modapreview .item-name').text(name);
        $('#modapreview .item-nameemail').text(email);
        $('#modapreview .card-text').html(text);



    })




});


var count = document.getElementsByClassName('num').length; //всего записей
var cnt = 25;
var cnt_page = Math.ceil(count / cnt); //кол-во страниц

var paginator = document.querySelector(".paginator");
var page = "";
for (var i = 0; i < cnt_page; i++) {
    page += "<span data-page=" + i * cnt + "  id=\"page" + (i + 1) + "\">" + (i + 1) + "</span>";
}
paginator.innerHTML = page;

var div_num = document.querySelectorAll(".num");
for (var i = 0; i < div_num.length; i++) {
    if (i < cnt) {
        div_num[i].style.display = "block";
    }
}

var main_page = document.getElementById("page1");
main_page.classList.add("paginator_active");


function pagination(event) {
    var e = event || window.event;
    var target = e.target;
    var id = target.id;

    if (target.tagName.toLowerCase() != "span") return;

    var num_ = id.substr(4);
    var data_page = +target.dataset.page;
    main_page.classList.remove("paginator_active");
    main_page = document.getElementById(id);
    main_page.classList.add("paginator_active");

    var j = 0;
    for (var i = 0; i < div_num.length; i++) {
        var data_num = div_num[i].dataset.num;
        if (data_num <= data_page || data_num >= data_page)
            div_num[i].style.display = "none";

    }
    for (var i = data_page; i < div_num.length; i++) {
        if (j >= cnt) break;
        div_num[i].style.display = "block";
        j++;
    }
}
